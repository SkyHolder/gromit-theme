# README

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for?

It is empty theme for October CMS.

Use it like start point of your amazing site

### How do I get set up?

just clone it in /themes/YourNameOfTheme/ and make it theme by default, all usefull things stay right on /demo page
after clone need run **npm install** for prepare Grunt tasks

this theme contain:

*   12-columns responsive grid (also support flex-grid and simple customize col count)
*   slick slider by [Ken Wheeler](http://kenwheeler.github.io/slick/)
*   css reset by [Eric A. Meyer](http://meyerweb.com/eric/tools/css/reset/)
*   modal
*   tabs
*   menu
*   set of buttons
*   lazy-load ( template in framework.less at 337 line)
*   forms reset

Contained files:

*   assets
    *   css
        *  app.less - for your amazimg styles
        *  fonts.less - for @font-face and text classes
        *  framework.less - grid, helpers, and other
        *  vars.less - base variables
            *  blocks - styles for partials from partials/blocks folder
            *  chips - styles for partials from partials/chips folder
            *  libs - vendor styles
                *  reset.css - small style reset
                *  slick-theme.less - style for slider
                *  slick.less - style for slider
            *  pages - stypes for pages
    *   fonts
    *   img
        *  svg - for any svg images
            *  icons - for svg icon wich use in svg sprite
    *   js
        *  libs - for vendor scripts
            *  jquery.arcticmodal-0.3.min.js - modal plugin
            *  jquery.lazy.min.js - lazy load plugin
            *  slick.min.js - slider plugin
*   layouts
*   pages
*   partials
    *   footer - empty sample of footer
    *   head - contain <head> tag foe style meta and other
    *   header - empty sample of footer
    *   modals - all modals must be here ( or not, as you wish )
    *   scripts - scripts file arrow for combiner
    *   styles - styles file arrow for combiner

**Grunt**

Tasks
*   svgstore - make svg sprite from all files in assets/img/svg/icons/
*   watch - looking for changes in icons directory
*   browserSync - looking for theme changes and reload browser

after grunt setup need change **browsersync** proxy option in **Gruntfile.js** with your site url and **name** property in **package.json** file


### Who do I talk to?

I'm commander [SkyHolder](https://vk.com/skyholder) and this is my favourite repository on the citadel