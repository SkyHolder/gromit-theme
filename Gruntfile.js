module.exports = function(grunt) {
    grunt.initConfig({
        svgstore: {
            options: {
                prefix : 'icon-',
                inheritviewbox: true,
                includeTitleElement: false,
                svg: {
                    style: 'display:none'
                }
            },
            default: {
                files: {
                    "assets/img/svg/svg-common-icon-defs.svg":["assets/img/svg/icons/*.svg"]
                }
            }
        },
        watch: {
            svg: {
                files:['assets/img/svg/icons/*.svg'],
                tasks: ['svgstore'],
            },
            config:{
                files:['Gruntfile.js'],
                tasks: ['svgstore'],
            }
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        './**/*.*'
                    ]
                },
                options: {
                    watchTask: true,
                    proxy: "siteurl",
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-svgstore');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.registerTask('default', ['svgstore','browserSync', 'watch']);
};